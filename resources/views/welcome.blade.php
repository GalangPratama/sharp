@extends('master.template')

@section('css')
    <link rel="stylesheet" href="{{ url('plugins/morris/morris.css') }}">
    
@endsection
@section('content')
    <div class="row">
        <div class="col-md-6 col-lg-6 col-xl-4">
            <a href="{{ route("shipmentKPI") }}" target="_blank">
            <div class="widget-bg-color-icon card-box fadeInDown animated">
                <div class="bg-icon bg-icon-info pull-left">
                    <i class="md md-local-shipping text-info"></i>
                </div>
                <div class="text-right">
                    <h3 class="text-dark" id="cb_allshipment"><b class="counter"></b></h3>
                    <p class="text-muted mb-0">All Shipment</p>
                </div>
                <div class="clearfix"></div>
            </div>
            </a>
        </div>

        <div class="col-md-6 col-lg-6 col-xl-4">
            <a href="{{ route("damageKPI") }}" target="_blank">
            <div class="widget-bg-color-icon card-box">
                <div class="bg-icon bg-icon-success pull-left">
                    <i class="md md-done-all text-success"></i>
                </div>
                <div class="text-right">
                    <h3 class="text-dark" id="cb_delivered"></h3>
                    <p class="text-muted mb-0">Delivered</p>
                </div>
                <div class="clearfix"></div>
            </div>
            </a>
        </div>

        {{-- <div class="col-md-6 col-lg-6 col-xl-4">
            <a href="#">
            <div class="widget-bg-color-icon card-box">
                <div class="bg-icon bg-icon-pink pull-left">
                    <i class="md md-refresh text-pink"></i>
                </div>
                <div class="text-right">
                    <h3 class="text-dark" id="cb_podreturn"></h3>
                    <p class="text-muted mb-0">POD Return</p>
                </div>
                <div class="clearfix"></div>
            </div>
            </a>
        </div> --}}

        <div class="col-md-6 col-lg-6 col-xl-4">
            <a href="#">
            <div class="widget-bg-color-icon card-box">
                <div class="bg-icon bg-icon-purple pull-left">
                    <i class="md md-payment text-purple"></i>
                </div>
                <div class="text-right">
                    <h3 class="text-dark" id="cb_invoicepaid"></h3>
                    <p class="text-muted mb-0">Invoice Paid</p>
                </div>
                <div class="clearfix"></div>
            </div>
            </a>
        </div>
    </div>

    <div class="row">
        {{-- <div class="col-lg-12 col-xl-12">
            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead style="text-align: center; background-color: #34D3EB;">
                    <tr>
                        <th style="text-align: center;">ALL AWB</th>
                        <th style="text-align: center;">AMOUNT</th>
                        <th style="text-align: center;">NY INVOICE</th>
                        <th style="text-align: center;">AMOUNT</th>
                        <th style="text-align: center;">INVOICE</th>
                        <th style="text-align: center;">AMOUNT</th>
                        <th style="text-align: center;">PAID</th>
                        <th style="text-align: center;">AMOUNT</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="text-align: center;" id="table_allawb"></td>
                        <td style="text-align: center;" id="table_amountallawb"></td>
                        <td style="text-align: center;" id="table_nyinvoice"></td>
                        <td style="text-align: center;" id="table_amountnyinvoice"></td>
                        <td style="text-align: center;" id="table_invoice"></td>
                        <td style="text-align: center;" id="table_amountinvoice"></td>
                        <td style="text-align: center;" id="table_paid"></td>
                        <td style="text-align: center;" id="table_amountpaid"></td>
                    </tr>
                </tbody>
            </table>
        </div> --}}

        <div class="col-lg-6 col-xl-6">
            <div class="card-box">
                <h4 class="text-dark header-title m-t-0">Shipment kpi</h4>
                {{-- <div class="row">
                    <div class="col-lg-8 col-xl-8"> --}}
                        <canvas id="shipmentkpi" style="height: 310px;"></canvas>
                    {{-- </div>
                    <div class="col-lg-3 col-xl-3" id="shp_kpi_rs1">
                        <h5 id="shp_kpi_rsn1"></h5>
                    </div>
                    <div class="col-lg-1 col-xl-1 text-center" id="shp_kpi_shp1">
                            <h5 id="shp_total1"></h5>
                    </div>
                </div> --}}
                <p  style="padding: 15px 0 30px 0; text-align: center; font-size: 14px;">
                    <a href="{{ route('shipmentKPI') }}" target="_blank">
                        {{-- <button class="btn btn-default text-dark m-t-0"> --}}
                            Show Detail <i class="ti-angle-double-right"></i>
                        {{-- </button> --}}
                    </a>
                </p>
            </div>
        </div>

        <div class="col-lg-6 col-xl-6">
            <div class="card-box">
                <h4 class="text-dark header-title m-t-0" style="padding: 15px 0 30px 0;">damage rate kpi</h4>
                {{-- <div class="row">
                    <div class="col-lg-8 col-xl-8"> --}}
                        <canvas id="damagekpi" style="height: 310px;"></canvas>
                    {{-- </div>
                    <div class="col-lg-3 col-xl-3" id="dmg_kpi_rsn">
                    </div>
                    <div class="col-lg-1 col-xl-1 text-center" id="dmg_kpi_shp">
                            <h5 id="dmg_total"></h5>
                    </div>
                </div> --}}
                <p  style="padding: 15px 0 30px 0; text-align: center; font-size: 14px;">
                    <a href="{{ route('damageKPI') }}" target="_blank">
                        {{-- <button class="btn btn-default text-dark m-t-0"> --}}
                            Show Detail <i class="ti-angle-double-right"></i>
                        {{-- </button> --}}
                    </a>
                </p>
            </div>
        </div>

        <div class="col-lg-6 col-xl-6">
            <div class="card-box">
                <h4 class="text-dark header-title m-t-0" style="padding: 15px 0 30px 0;">all shipment <span id="dateAllShipment"></span></h4>
                <div class="row">
                    <div class="col-lg-12 col-xl-12">
                        <canvas id="allshipment" style="height: 310px;"></canvas>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-xl-6">
            <div class="card-box">
                <h4 class="text-dark header-title m-t-0" style="padding: 15px 0 30px 0;">Shipment by origin</h4>
                <div class="row">
                    <div class="col-lg-12 col-xl-12">
                        <canvas id="shipmentbyorigin" style="height: 310px;"></canvas>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-xl-12">
            <div class="card-box">
                <h4 class="text-dark header-title m-t-0" style="padding: 15px 0 30px 0;">daily shipment</h4>
                <div class="row">
                    <div class="col-lg-12 col-xl-12">
                        <canvas id="dailyshipment" style="height: 310px;"></canvas>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-xl-12">
            <div class="card-box">
                <h4 class="text-dark header-title m-t-0" style="padding: 15px 0 30px 0;">Shipment by destination</h4>
                {{-- <div class="text-center">
                    <ul class="list-inline chart-detail-list">
                        <li class="list-inline-item">
                            <h5><i class="fa fa-circle m-r-5" style="color: #ade498;"></i>Achieve</h5>
                        </li>
                        <li class="list-inline-item">
                            <h5><i class="fa fa-circle m-r-5" style="color: #cf1b1b;"></i>Failed</h5>
                        </li>
                    </ul>
                </div> --}}
                <div class="row">
                    <div class="col-lg-12 col-xl-12">
                        <canvas id="shipmentbydestination" style="height: 310px;"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- end row -->
<div class="clearfix" id="loading"> 
    <div class="loader">
        <img src="{{ url('assets/images/sp-loading2.gif') }}" alt="loading">
    </div>
</div>
   
@endsection

@section('js')
    <script src="{{ url('plugins/chart.js/chart.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            
                
            var APP_URL = {!! json_encode(url('/')) !!};

            var DefaultTglAwal = $('#from_filter_date').val();
            var DefaultTglAkhir = $('#to_filter_date').val();
            var shipmentkpi = document.getElementById('shipmentkpi').getContext('2d');
            var damagekpi = document.getElementById('damagekpi').getContext('2d');
            var allshipment = document.getElementById('allshipment').getContext('2d');
            var dailyshipment = document.getElementById('dailyshipment').getContext('2d');
            var shipmentbyorigin = document.getElementById('shipmentbyorigin').getContext('2d');
            var shipmentbydestination = document.getElementById('shipmentbydestination').getContext('2d');
            // var invoicestatus = document.getElementById('ivcsts').getContext('2d');

            var token = '00043eb6617434cc5f357bbf692e53b3';
            var category = $('#category').val();
            var customerID = "{{ Session::get('agent_vnid') }}";
            
                function Rupiah(angka){
                    var uang = Math.round(angka);
                    var	number_string = uang.toString(),
                    sisa 	= number_string.length % 3,
                                rupiah 	= number_string.substr(0, sisa),
                                ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
                                    
                            if (ribuan) {
                                separator = sisa ? ',' : '';
                                rupiah += separator + ribuan.join(',');
                            }
                    return rupiah;
                }
                
                $('#FilterDate').click(function(){
                    var tglAwal = $('#from_filter_date').val();
                    var tglAkhir = $('#to_filter_date').val();
                    load_data(tglAwal, tglAkhir);
                    $('#Filter-modal').modal('hide');
                })

                load_data();
                    function load_data(tglAwal = DefaultTglAwal, tglAkhir = DefaultTglAkhir){
                        $(document).ajaxStart(function() {
                            $("#loading").show();
                            }).ajaxStop(function() {
                            $("#loading").hide();
                        });

                        $.ajax({
                            url: APP_URL+'/datadashboard',
                            method: 'GET',
                            dataType: 'json',
                            data: {'accountID': customerID, 'category': category, 'from': tglAwal, 'to': tglAkhir},
                            success: function (d) {
                                var table = d.tbdata;
                                $('#cb_allshipment').html('<b class="counter">'+d.shipment+'</b>');
                                $('#cb_delivered').html('<b class="counter">'+d.pod+'</b>');
                                $('#cb_podreturn').html('<b class="counter">'+d.returned+'</b>/'+d.pod);
                                $('#cb_invoicepaid').html('<b class="counter">'+d.paid+'</b>');
                                $.each(table, function(i, val){
                                    $('#table_allawb').html(val.allawb);                           
                                    $('#table_amountallawb').html(Rupiah(val.ttlawb));
                                    $('#table_nyinvoice').html(val.awbnyinv);                           
                                    $('#table_amountnyinvoice').html(Rupiah(val.ttlnyinv));
                                    $('#table_invoice').html(val.awbinv);                           
                                    $('#table_amountinvoice').html(Rupiah(val.ttlinv));
                                    $('#table_paid').html(val.awbpaid);                           
                                    $('#table_amountpaid').html(Rupiah(val.ttlpaid));
                                })
                            },
                            error: function(d){
                                alert("Connection Error");
                            }
                        })

                        $.ajax({
                            url: APP_URL+'/datashpkpi',
                            method: 'GET',
                            dataType: 'json',
                            data: {'accountID': customerID, 'category': category, 'from': tglAwal, 'to': tglAkhir},
                            success: function (d) {
                                var tes = Array(d.success, d.failed, d.ondel);
                                var total = d.total;
                                new Chart(shipmentkpi, {
                                    type: 'bar',
                                    data: {
                                        ykeys: ['Achieve', 'Failed', 'On Delivery'],
                                        labels: ['Achieve', 'Failed', 'On Delivery'],
                                        datasets: [{
                                            label: '',
                                            data: Object.values(tes),
                                            backgroundColor: [
                                                '#ade498',
                                                '#f4668f',
                                                '#808088',
                                            ],
                                            borderColor: [
                                                '#ade498',
                                                '#f4668f',
                                                '#808088',
                                            ],
                                            borderWidth: 1
                                        }]
                                    },
                                    options: {
                                        legend: {
                                            display: false
                                        },
                                        scales: {
                                            yAxes: [{
                                                ticks: {
                                                    beginAtZero: true,
                                                    min: 0,
                                                    max: 500,
                                                    stepSize: 100,
                                                }
                                            }],
                                            xAxes: [{
                                                // Change here
                                                barPercentage: 0.5
                                            }]
                                        },
                                        animation: {
                                        duration: 1,
                                        onComplete: function() {
                                            var chartInstance = this.chart,
                                            ctx = chartInstance.ctx;

                                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                                            ctx.textAlign = 'center';
                                            ctx.textBaseline = 'bottom';

                                            this.data.datasets.forEach(function(dataset, i) {
                                            var meta = chartInstance.controller.getDatasetMeta(i);
                                            meta.data.forEach(function(bar, index) {
                                                if (dataset.data[index] > 0) {
                                                var data = dataset.data[index];
                                                ctx.fillText(data, bar._model.x, bar._model.y);
                                                }
                                            });
                                            });
                                        }
                                        },
                                        tooltips: {       
                                                callbacks: {
                                                    title: function(tooltipItem, data) {
                                                        return '';
                                                    },
                                                    label: function(tooltipItem, data) {
                                                        return 'Qty : ' + data['datasets'][0]['data'][tooltipItem['index']];
                                                    },
                                                    afterLabel: function(tooltipItem, data) {
                                                        var percent = Math.round(data['datasets'][0]['data'][tooltipItem['index']] / total * 100).toFixed(2);
                                                        return '(' + percent + '%)';
                                                    },
                                                },
                                        position: 'average',
                                        xPadding: 15,
                                        yPadding: 15,
                                        },
                                    },
                                })
                                $('#shp_total1').html(d.total);
                            }
                        })

                        $.ajax({
                            url: APP_URL+'/datadmgkpi',
                            method: 'GET',
                            dataType: 'json',
                            data: {'accountID': customerID, 'category': category, 'from': tglAwal, 'to': tglAkhir},
                            success: function (d) {

                                var datadmg = d.data;
                                $.each(datadmg, function(dataType, data){
                                var total = data.success;
                                    new Chart(damagekpi, {
                                        type: 'bar',
                                        data: {
                                            ykeys: ['Good', 'Damage'],
                                            labels: ['Good', 'Damage'],
                                            datasets: [{
                                                label: '',
                                                data: Object.values(data),
                                                backgroundColor: [
                                                    '#ade498',
                                                    '#f4668f',
                                                ],
                                                borderColor: [
                                                    '#ade498',
                                                    '#f4668f',
                                                ],
                                                borderWidth: 1
                                            }]
                                        },
                                        options: {
                                            legend: {
                                                display: false
                                            },
                                            scales: {
                                                yAxes: [{
                                                    ticks: {
                                                        beginAtZero: true,
                                                        min: 0,
                                                        max: 1000,
                                                        stepSize: 200,
                                                    }
                                                }],
                                                xAxes: [{
                                                    // Change here
                                                    barPercentage: 0.5
                                                }]
                                            },
                                            animation: {
                                            duration: 1,
                                            onComplete: function() {
                                                var chartInstance = this.chart,
                                                ctx = chartInstance.ctx;

                                                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                                                ctx.textAlign = 'center';
                                                ctx.textBaseline = 'bottom';

                                                this.data.datasets.forEach(function(dataset, i) {
                                                var meta = chartInstance.controller.getDatasetMeta(i);
                                                meta.data.forEach(function(bar, index) {
                                                    if (dataset.data[index] > 0) {
                                                    var data = dataset.data[index];
                                                    ctx.fillText(data, bar._model.x, bar._model.y);
                                                    }
                                                });
                                                });
                                            }
                                            },
                                            tooltips: {       
                                                    callbacks: {
                                                        title: function(tooltipItem, data) {
                                                            return '';
                                                        },
                                                        label: function(tooltipItem, data) {
                                                            return 'Qty : ' + data['datasets'][0]['data'][tooltipItem['index']];
                                                        },
                                                        afterLabel: function(tooltipItem, data) {
                                                            var percent = Math.round(data['datasets'][0]['data'][tooltipItem['index']] / total * 100).toFixed(2);
                                                            return '(' + percent + '%)';
                                                        },
                                                    },
                                            position: 'average',
                                            xPadding: 15,
                                            yPadding: 15,
                                            },
                                        },
                                    })
                                })

                                var total = d.data;
                                $.each(total, function(dataType, data){
                                    $('#dmg_total').html(data.success);
                                });
                            }
                        })

                        $.ajax({
                            url: APP_URL+'/datagrapall',
                            method: 'GET',
                            dataType: 'json',
                            data: {'accountID': customerID, 'category': category, 'from': tglAwal, 'to': tglAkhir},
                            success: function (d) {
                                $.each(d, function(dataType, data){
                                    var total = data.total;
                                        new Chart(allshipment, {
                                            type: 'doughnut',
                                            data: {
                                                labels: ['Process', 'Delivery'],
                                                datasets: [{
                                                    label: '',
                                                    data: [data.processs, data.pod],
                                                    backgroundColor: [
                                                        '#5D9CEC',
                                                        '#ade498',
                                                    ],
                                                    borderColor: [
                                                        '#5D9CEC',
                                                        '#ade498',
                                                    ],
                                                    borderWidth: 1
                                                }]
                                            },
                                            options: {
                                                plugins: {
                                                    labels: [
                                                        {
                                                        render: 'value',
                                                        position: 'outside'
                                                        },
                                                        {
                                                        render: 'percentage'
                                                        }
                                                    ]
                                                },
                                                    // animation: {
                                                    //     duration: 1,
                                                    //     onComplete: function() {
                                                    //         var chartInstance = this.chart,
                                                    //         ctx = chartInstance.ctx;

                                                    //         ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                                                    //         // ctx.textAlign = 'center';
                                                    //         // ctx.textBaseline = 'bottom';

                                                    //         this.data.datasets.forEach(function(dataset, i) {
                                                    //         var meta = chartInstance.controller.getDatasetMeta(i);
                                                    //         meta.data.forEach(function(bar, index) {
                                                    //             if (dataset.data[index] > 0) {
                                                    //             var data = dataset.data[index];
                                                    //             ctx.fillText(data, bar._model.x, bar._model.y);
                                                    //             }
                                                    //         });
                                                    //         });
                                                    //     }
                                                    // },
                                                tooltips: {       
                                                        callbacks: {
                                                            title: function(tooltipItem, data) {
                                                                return '';
                                                            },
                                                            label: function(tooltipItem, data) {
                                                                return 'Qty : ' + data['datasets'][0]['data'][tooltipItem['index']];
                                                            },
                                                            afterLabel: function(tooltipItem, data) {
                                                                var percent = Math.round(data['datasets'][0]['data'][tooltipItem['index']] / total * 100).toFixed(2);
                                                                return '(' + percent + '%)';
                                                            },
                                                        },
                                                    position: 'average',
                                                    xPadding: 15,
                                                    yPadding: 15,
                                                },
                                            },
                                        });

                                        $('#dateAllShipment').html("(" + data.tahun +")");
                                })
                            }
                        })

                        $.ajax({
                            url: APP_URL+'/datagraporg',
                            method: 'GET',
                            dataType: 'json',
                            data: {'accountID': customerID, 'category': category, 'from': tglAwal, 'to': tglAkhir},
                            success: function (d) {
                                
                                var total = d.total;
                                var date = [];
                                var data1 = [];
                                var data2 = [];

                                for(var i in d) {
                                    date.push(d[i].trnOrg);
                                    data1.push(d[i].pod);
                                    data2.push(d[i].processs);
                                }
                                var chartdata = {
                                    labels: date,
                                    datasets: [
                                        {
                                            backgroundColor: '#ade498',
                                            borderColor: '#ade498',
                                            hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
                                            hoverBorderColor: 'rgba(200, 200, 200, 1)',
                                            data: data1,
                                            fill: false,
                                        },
                                        {
                                            backgroundColor: '#f4668f',
                                            borderColor: '#f4668f',
                                            hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
                                            hoverBorderColor: 'rgba(200, 200, 200, 1)',
                                            data: data2,
                                            fill: false,
                                        }
                                    ]
                                };
                                        new Chart(shipmentbyorigin, {
                                            type: 'bar',
                                            data: chartdata,
                                            options: {
                                            legend: {
                                                display: false
                                            },
                                            scales: {
                                                yAxes: [{
                                                    ticks: {
                                                        beginAtZero: true,
                                                        min: 0,
                                                        max: 500,
                                                        stepSize: 100,
                                                    }
                                                }],
                                                xAxes: [{
                                                    // Change here
                                                    barPercentage: 0.5
                                                }]
                                            },
                                            animation: {
                                            duration: 1,
                                            onComplete: function() {
                                                var chartInstance = this.chart,
                                                ctx = chartInstance.ctx;

                                                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                                                ctx.textAlign = 'center';
                                                ctx.textBaseline = 'bottom';

                                                this.data.datasets.forEach(function(dataset, i) {
                                                var meta = chartInstance.controller.getDatasetMeta(i);
                                                meta.data.forEach(function(bar, index) {
                                                    if (dataset.data[index] > 0) {
                                                    var data = dataset.data[index];
                                                    ctx.fillText(data, bar._model.x, bar._model.y);
                                                    }
                                                });
                                                });
                                            }
                                            },
                                            tooltips: {       
                                                    callbacks: {
                                                        title: function(tooltipItem, data) {
                                                            return '';
                                                        },
                                                        label: function(tooltipItem, data) {
                                                            return 'Qty : ' + data['datasets'][0]['data'][tooltipItem['index']];
                                                        },
                                                        afterLabel: function(tooltipItem, data) {
                                                            var percent = Math.round(data['datasets'][0]['data'][tooltipItem['index']] / total * 100).toFixed(2);
                                                            return '(' + percent + '%)';
                                                        },
                                                    },
                                            position: 'average',
                                            xPadding: 15,
                                            yPadding: 15,
                                            },
                                            },
                                        });
                            }
                        })

                        $.ajax({
                            url: APP_URL+'/datagrapdealy',
                            method: 'GET',
                            dataType: 'json',
                            data: {'accountID': customerID, 'category': category, 'from': tglAwal, 'to': tglAkhir},
                            success: function (d) {
                                var date = [];
                                var data1 = [];
                                var data2 = [];

                                for(var i in d) {
                                    date.push(d[i].trnDate);
                                    data1.push(d[i].pod);
                                    data2.push(d[i].processs);
                                }

                                var chartdata = {
                                    labels: date,
                                    datasets: [
                                        {
                                            backgroundColor: '#ade498',
                                            borderColor: '#ade498',
                                            hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
                                            hoverBorderColor: 'rgba(200, 200, 200, 1)',
                                            data: data1,
                                            fill: false,
                                        },
                                        {
                                            backgroundColor: '#f4668f',
                                            borderColor: '#f4668f',
                                            hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
                                            hoverBorderColor: 'rgba(200, 200, 200, 1)',
                                            data: data2,
                                            fill: false,
                                        }
                                    ]
                                };
                                new Chart(dailyshipment, {
                                                type: 'line',
                                                data: chartdata,
                                                options: {
                                                    legend: {
                                                        display: false
                                                    },
                                                    scales: {
                                                        yAxes: [{
                                                            ticks: {
                                                                beginAtZero: true,
                                                                min: 0,
                                                                max: 120,
                                                                stepSize: 20,
                                                            }
                                                        }],
                                                        xAxes: [{
                                                            // Change here
                                                            barPercentage: 0.2
                                                        }]
                                                    },
                                                    tooltips: {       
                                                            callbacks: {
                                                                title: function(tooltipItem, data) {
                                                                    return '';
                                                                },
                                                                label: function(tooltipItem, data) {
                                                                    return 'Qty : ' + data['datasets'][0]['data'][tooltipItem['index']];
                                                                },
                                                            },
                                                    position: 'average',
                                                    xPadding: 15,
                                                    yPadding: 15,
                                                    },
                                                },
                                });
                            }
                        })

                        $.ajax({
                            url: APP_URL+'/datashpdst',
                            method: 'GET',
                            dataType: 'json',
                            data: {'accountID': customerID, 'category': category, 'from': tglAwal, 'to': tglAkhir},
                            success: function (d) {
                                var dest = [];
                                var data1 = [];
                                var data2 = [];

                                for(var i in d) {
                                    dest.push(d[i].trnDest);
                                    data1.push(d[i].pod);
                                    data2.push(d[i].processs);
                                }

                                var chartdata = {
                                    labels: dest,
                                    datasets: [
                                        {
                                            backgroundColor: '#ade498',
                                            borderColor: '#ade498',
                                            hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
                                            hoverBorderColor: 'rgba(200, 200, 200, 1)',
                                            data: data1,
                                            fill: false,
                                        },
                                        {
                                            backgroundColor: '#f4668f',
                                            borderColor: '#f4668f',
                                            hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
                                            hoverBorderColor: 'rgba(200, 200, 200, 1)',
                                            data: data2,
                                            fill: false,
                                        }
                                    ]
                                };
                            new Chart(shipmentbydestination, {
                                type: 'line',
                                data: chartdata,
                                            options: {
                                                    legend: {
                                                        display: false
                                                    },
                                                    scales: {
                                                        yAxes: [{
                                                            ticks: {
                                                                beginAtZero: true,
                                                                min: 0,
                                                                max: 50,
                                                                stepSize: 10,
                                                            }
                                                        }],
                                                        xAxes: [{
                                                            // Change here
                                                            barPercentage: 0.2
                                                        }]
                                                    },
                                                    animation: {
                                                    duration: 1,
                                                    // onComplete: function() {
                                                    //     var chartInstance = this.chart,
                                                    //     ctx = chartInstance.ctx;

                                                    //     ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                                                    //     ctx.textAlign = 'center';
                                                    //     ctx.textBaseline = 'bottom';

                                                    //     this.data.datasets.forEach(function(dataset, i) {
                                                    //     var meta = chartInstance.controller.getDatasetMeta(i);
                                                    //     meta.data.forEach(function(bar, index) {
                                                    //         if (dataset.data[index] > 0) {
                                                    //         var data = dataset.data[index];
                                                    //         ctx.fillText(data, bar._model.x, bar._model.y);
                                                    //         }
                                                    //     });
                                                    //     });
                                                    // }
                                                    },
                                                    tooltips: {       
                                                            callbacks: {
                                                                title: function(tooltipItem, data) {
                                                                    return '';
                                                                },
                                                                label: function(tooltipItem, data) {
                                                                    return 'Qty : ' + data['datasets'][0]['data'][tooltipItem['index']];
                                                                },
                                                            },
                                                    position: 'average',
                                                    xPadding: 15,
                                                    yPadding: 15,
                                                    },
                                            },

                            });
                            }
                        })

                    }

            
                    
        });
        
</script>
@endsection