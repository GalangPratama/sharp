<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="shortcut icon" href="{{ url('assets/images/users/avatar-12.png') }}">

        <title>TMS | {{ $title }}</title>

        <!--Morris Chart CSS -->
        <link href="{{ url('plugins/custombox/css/custombox.css') }}" rel="stylesheet">
        <link href="{{ url('plugins/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
        <link href="{{ url('plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
        <link href="{{ url('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
        <link href="{{ url('plugins/clockpicker/css/bootstrap-clockpicker.min.css') }}" rel="stylesheet">
        <link href="{{ url('plugins/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
        <link href="{{ url('plugins/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
        <link href="{{ url('plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
        <link href="{{ url('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
        <link href="{{ url('plugins/clockpicker/css/bootstrap-clockpicker.min.css') }}" rel="stylesheet">
        <link href="{{ url('plugins/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
        <link href="{{ url('plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
        <!-- DataTables -->
        <link href="{{ url('plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/css/style.css') }}" rel="stylesheet" type="text/css" />

        <script src="{{ url('assets/js/modernizr.min.js') }}" type="text/css"></script>


    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
                <div class="topbar">

                    <!-- LOGO -->
                    <div class="topbar-left">
                        <div class="text-center">
                            {{-- <a href="index.html" class="logo"><img src="{{ url('assets/images/logo-total.png') }}" alt="" width="200"></a> --}}
                            <!-- Image Logo here -->
                        {{-- <a href="index.html" class="logo">
                            <i class="icon-c-logo"><h1 style="height: 100%; padding: 13px 0; letter-spacing: 8px; font-family:monospace">T</h1></i>
                            <span><h1 style="height: 100%; padding: 13px 0; letter-spacing: 8px; font-family:monospace">TMS</h1></span>
                            
                        </a> --}}
                        </div>
                    </div>

                    <!-- Button mobile view to collapse sidebar menu -->
                    <nav class="navbar-custom">

                        <ul class="list-inline float-right mb-0">
                            
                            <li class="list-inline-item notification-list">
                                <a class="nav-link waves-light waves-effect" href="#" id="btn-fullscreen">
                                    <i class="dripicons-expand noti-icon"></i>
                                </a>
                            </li>
                            <li class="list-inline-item dropdown notification-list">
                                <a href="#" class="btn waves-effect waves-light" data-toggle="modal" data-target="#Filter-modal">
                                <i class="dripicons-gear noti-icon"></i>
                                </a>
                            </li>
                            <li class="list-inline-item dropdown notification-list" style="color: #fff;" id="clock">
                            </li>

                        </ul>

                        <ul class="list-inline menu-left mb-0 text-center">
                            <img src="{{ url('assets/images/tl.png') }}" alt="nissan" width="200"> 
                        </ul>

                    </nav>

                </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
                {{-- @include('master.sidebar') --}}
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->

                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-xl-12">
                                    <form action="#" method="post">
                                        <div class="widget-bg-color-icon card-box" id="card-shipment">
                                            
                                            <div class="row">
                                                <div class="col-lg-6 button_report" style="margin-bottom: 30px; text-align: left; padding-right: 20px;">
                                                    <button type="button" id="shpkpi_print" class="btn btn-custom2">Print <i class="fa fa-print"></i></button>
                                                    <button type="button" id="shpkpi_excel" class="btn btn-custom2">Export <i class="fa fa-file-excel-o"></i></button>
                                                </div>

                                                <div class="col-lg-6  search" style="margin-bottom: 30px; text-align: right; padding-right: 20px;">
                                                    <input type="text" class="form-input" name="search-table" id="search-table" placeholder="Search">
                                                </div>
                                                <div class="col-lg-12">
                                                    <table id="shipmentkpi" class="table table-striped" cellspacing="0" width="100%">
                                                        <thead style="text-align: center; background-color: #D6F6FB;">
                                                        <tr>
                                                            <th style="text-align: center;">AWB</th>
                                                            <th style="text-align: center;">Collie</th>
                                                            <th style="text-align: center;">Pickup Date</th>
                                                            <th style="text-align: center;">From</th>
                                                            <th style="text-align: center;">Destination</th>
                                                            <th style="text-align: center;">Packing</th>
                                                            <th style="text-align: center;">No. DO WMS</th>
                                                            <th style="text-align: center;">Kg</th>
                                                            <th style="text-align: center;">Received Date</th>
                                                            <th style="text-align: center;">Penerima</th>
                                                            <th style="text-align: center;">Propose</th>
                                                            <th style="text-align: center;">Actual</th>
                                                            <th style="text-align: center;">KPI</th>
                                                            <th style="text-align: center;">Log DESC</th>
                                                            <th style="text-align: center;">KETERANGAN</th>
                                                        </tr>
                                                        </thead>
    
    
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                            
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                        <div class="clearfix" id="loading"> 
                                            <div class="loader">
                                                <img src="{{ url('assets/images/sp-loading2.gif') }}" alt="loading">
                                            </div>
                                        </div>
                                </div> 
                            </div>



                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer">
                    Copyright &copy; 2020 by <a href="http://tora.co.id/" target="_blank">TORA Development Team.</a> 
                </footer>



            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

        </div>
        <!-- END wrapper -->

        

        <div class="modal fade" id="Filter-modal" role="dialog">
            <div class="modal-dialog">
         
             <!-- Modal content-->
                <div class="modal-content">
                    <form id="FilterForm">
                        <div class="modal-header" style="text-align: center;">
                            <h4 class="modal-title">Filter Report</h4>
                            <button type="button" class="close" data-dismiss="modal"></button>
                        </div>
                        <div class="modal-text">
                            <div class="row">
                                <div class="col-md-12" style="display: none;">
                                    <h5>Select Category</h5>
                                    <select class="form-control" name="category" id="category">
                                        <option value="all">All</option>
                                        <option value="">Consumable</option>
                                        <option value="">Demo</option>
                                        <option value="">Device</option>
                                        <option value="">Other</option>
                                    </select>    
                                </div>
                                <div class="col-md-12 row" style="margin-top: 20px;">
                                    <div class="col-md-12">
                                        <h5>Select Periode</h5>
                                    </div>
                                    <div class="input-group col-md-6">
                                        <input type="text" class="form-control" placeholder="From" id="from_filter_date">
                                        <span class="input-group-addon bg-custom b-0"><i class="md md-event-note text-white"></i></span>
                                    </div><!-- input-group -->
                                
                                    <div class="input-group col-md-6">
                                        <input type="text" class="form-control" placeholder="To" id="to_filter_date">
                                        <span class="input-group-addon bg-custom b-0"><i class="md md-event-note text-white"></i></span>
                                    </div><!-- input-group -->
                                </div>
                            </div>
                        </div>
                    </form>
                    
                    <div class="modal-footer">
                        <button class="btn btn-default" id="FilterDate">Filter <i class="md-tune"></i></button>
                    </div>
                </div>
            </div>
        </div>

        
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="{{ url('assets/js/jquery.min.js') }}"></script>
        <script src="{{ url('assets/js/popper.min.js') }}"></script><!-- Popper for Bootstrap -->
        <script src="{{ url('assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ url('assets/js/detect.js') }}"></script>
        <script src="{{ url('assets/js/fastclick.js') }}"></script>
        <script src="{{ url('assets/js/jquery.slimscroll.js') }}"></script>
        <script src="{{ url('assets/js/jquery.blockUI.js') }}"></script>
        <script src="{{ url('assets/js/waves.js') }}"></script>
        <script src="{{ url('assets/js/wow.min.js') }}"></script>
        <script src="{{ url('assets/js/jquery.nicescroll.js') }}"></script>
        <script src="{{ url('assets/js/jquery.scrollTo.min.js') }}"></script>
        <script src="{{ url('plugins/peity/jquery.peity.min.js') }}"></script>        
        
        <!-- jQuery  -->
        {{-- <script src="{{ url('plugins/waypoints/lib/jquery.waypoints.min.js') }}"></script>
        <script src="{{ url('plugins/counterup/jquery.counterup.min.js') }}"></script>
        <script src="{{ url('plugins/jquery-knob/jquery.knob.js') }}"></script> --}}
        <script src="{{ url('assets/js/jquery.core.js') }}"></script>
        <script src="{{ url('assets/js/jquery.app.js') }}"></script>

         <script src="{{ url('plugins/moment/moment.js') }}"></script>
        <script src="{{ url('plugins/timepicker/bootstrap-timepicker.js') }}"></script>
        <script src="{{ url('plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
        <script src="{{ url('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ url('plugins/clockpicker/js/bootstrap-clockpicker.min.js') }}"></script>
        <script src="{{ url('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
        <script src="{{ url('assets/pages/jquery.form-pickers.init.js') }}"></script>
        <script src="{{ url('plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js') }}"></script>
        <script src="{{ url('plugins/switchery/js/switchery.min.js') }}"></script>
        <script type="text/javascript" src="{{ url('plugins/multiselect/js/jquery.multi-select.js') }}"></script>
        <script type="text/javascript" src="{{ url('plugins/jquery-quicksearch/jquery.quicksearch.js') }}"></script>
        <script src="{{ url('plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>

        <script type="text/javascript" src="{{ url('assets/pages/jquery.form-advanced.init.js') }}"></script>
        <!-- Required datatable js -->
        <script src="{{ url('plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ url('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <!-- Buttons examples -->
        <script src="{{ url('plugins/datatables/dataTables.buttons.min.js') }}"></script>
        <script src="{{ url('plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
        <script src="{{ url('plugins/datatables/jszip.min.js') }}"></script>
        <script src="{{ url('plugins/datatables/pdfmake.min.js') }}"></script>
        <script src="{{ url('plugins/datatables/vfs_fonts.js') }}"></script>
        <script src="{{ url('plugins/datatables/buttons.html5.min.js') }}"></script>
        <script src="{{ url('plugins/datatables/buttons.print.min.js') }}"></script>
        <script src="{{ url('plugins/datatables/buttons.colVis.min.js') }}"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                var APP_URL = {!! json_encode(url('/')) !!};

                var DefaultTglAwal = $('#from_filter_date').val();
                var DefaultTglAkhir = $('#to_filter_date').val();
                var accountID = "{{ Session::get('agent_vnid') }}";
                var category = $('#category').val();


                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $(document).ajaxStart(function() {
                    $("#loading").show();
                    }).ajaxStop(function() {
                    $("#loading").hide();
                });
                load_data();

                function load_data(tglAwal = DefaultTglAwal, tglAkhir = DefaultTglAkhir)
                {
                            var shipmentkpi = $('#shipmentkpi').DataTable({
                                        processing: true,
                                        serverside: true,
                                        searching: true,
                                        ordering: false,
                                        destroy: true,
                                        dom: 'lrtip',
                                        dom: 'Bfrtip',
                                        buttons: [
                                            {
                                            extend: 'excel',
                                            autoFilter: true,            
                                            },
                                            {
                                            extend: 'print',
                                            autoFilter: true,               
                                            }
                                        ],
                                        ajax: {
                                            method : 'GET',
                                            url : APP_URL+"/datashpkpi",
                                            dataType : 'json',
                                            dataSrc: "data",
                                            data: {'accountID': accountID, 'category': category, 'from': tglAwal, 'to': tglAkhir},
                                        },
                                        columns: [
                                            {
                                                data:'trnNoHAWB'
                                            },
                                            {
                                                data:'trnkoli'
                                            },
                                            {
                                                data:'trnPickupDate'
                                            },
                                            {
                                                data:'trnOrg'
                                            },
                                            {
                                                data:'trnDest'
                                            },
                                            {
                                                data:'trnpacking'
                                            },
                                            {
                                                data:'trncustref'
                                            },
                                            {
                                                data:'trnchargeswt'
                                            },
                                            {
                                                data:'trnDeliveredDate'
                                            },
                                            {
                                                data:'trnDeliveredByName'
                                            },
                                            {
                                                data:'leadtime'
                                            },
                                            {
                                                data:'sla_act'
                                            },
                                            {
                                                data:'sla_kpi'
                                            },
                                            {
                                                data:'log_desc'
                                            },
                                            {
                                                data:'trnnoted'
                                            }
                                        ],
                                        columnDefs: [
                                            {"className": "dt-center", "targets": "_all"}
                                        ]
                            });
                
                            $('#search-table').keyup(function(){
                                shipmentkpi.search($(this).val()).draw() ;
                            })
                    $("#shpkpi_excel").on("click", function() {
                        shipmentkpi.button( '.buttons-excel' ).trigger();
                    });
                    $("#shpkpi_print").on("click", function() {
                        shipmentkpi.button( '.buttons-print' ).trigger();
                    }); 
                }
                
                $('#FilterDate').click(function(){
                    var tglAwal = $('#from_filter_date').val();
                    var tglAkhir = $('#to_filter_date').val();
                        $('#shipmentkpi').DataTable().destroy();
                        load_data(tglAwal, tglAkhir);
                    $('#Filter-modal').modal('hide');
                })
                            

               
            });

        </script>

        <script src="{{ url('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ url('plugins/custombox/js/custombox.min.js') }}"></script>
        <script src="{{ url('plugins/custombox/js/legacy.min.js') }}"></script>
        <script type="text/javascript">
            // jQuery(document).ready(function($) {
            //     $('.counter').counterUp({
            //         delay: 100,
            //         time: 1200
            //     });

            //     $(".knob").knob();

            // });
            // function addZero(i) {
            //     if (i < 10) {
            //         i = "0" + i;
            //     }
            //     return i;
            // }
        
            function time(){
                var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                var date = new Date();
                var date1 = date.getDate();
                var date2 = months[date.getMonth()];
                var date3 = date.getFullYear();
                var hours = addZero(date.getHours());
                var minuts = addZero(date.getMinutes());
                var second = addZero(date.getSeconds());

                document.getElementById('clock').innerHTML = date1+" "+date2+" "+date3+" "+hours+":"+minuts+":"+second;
                setTimeout(time, 1000);
                return true;
            }
                jQuery('#datepicker').datepicker();
                jQuery('#from_filter_date').datepicker({
					format: "yyyy-mm-dd",
                	autoclose: true,
                	todayHighlight: true
				});

				jQuery('#to_filter_date').datepicker({
					format: "yyyy-mm-dd",
                	autoclose: true,
                    todayHighlight: true,                    
                });
                var d = new Date();
                var currMonth = d.getMonth();
                var currYear = d.getFullYear();
                var startDate = new Date(currYear, currMonth, 1);
                var endDate = new Date(currYear, currMonth, 30);

                jQuery('#from_filter_date').datepicker("setDate", startDate);
                jQuery('#to_filter_date').datepicker("setDate", endDate);
        </script>

    </body>
</html>