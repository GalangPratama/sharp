@extends('master.template')

@section('css')
    <link href="{{ url('plugins/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
    <link href="{{ url('plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
    <link href="{{ url('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ url('plugins/clockpicker/css/bootstrap-clockpicker.min.css') }}" rel="stylesheet">
    <link href="{{ url('plugins/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
    <link href="{{ url('plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
    <!-- DataTables -->
    <link href="{{ url('plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-lg-12 col-xl-12">
        <form action="#" method="post">
            <div class="widget-bg-color-icon card-box">
                <div class="col-12 button_report" style="margin-bottom: 30px; text-align: right; padding-right: 20px;">
                    <button type="button" id="dmgkpi_print" class="btn btn-custom2">Print <i class="fa fa-print"></i></button>
                    <button type="button" id="dmgkpi_excel" class="btn btn-custom2">Export <i class="fa fa-file-excel-o"></i></button>
                </div>

                <div class="col-12">
                    <table id="dmgkpi" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead style="text-align: center; background-color: #D6F6FB;">
                        <tr>
                            <th style="text-align: center;">No</th>
                            <th style="text-align: center;">AWB</th>
                            <th style="text-align: center;">ORG</th>
                            <th style="text-align: center;">DST</th>
                            <th style="text-align: center;">Pickup Date</th>
                            <th style="text-align: center;">Weight</th>
                            <th style="text-align: center;">Koli</th>
                            <th style="text-align: center;">KPI</th>
                        </tr>
                        </thead>


                        <tbody>
                        </tbody>
                    </table>
                            
                </div>
            </div>
        </form>
            <div class="clearfix" id="loading"> 
                <div class="loader">
                    <img src="{{ url('assets/images/sp-loading2.gif') }}" alt="loading">
                </div>
            </div>
    </div> 
</div>

</div>

@endsection

@section('js')
    <script src="{{ url('plugins/moment/moment.js') }}"></script>
    <script src="{{ url('plugins/timepicker/bootstrap-timepicker.js') }}"></script>
    <script src="{{ url('plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{ url('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ url('plugins/clockpicker/js/bootstrap-clockpicker.min.js') }}"></script>
    <script src="{{ url('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ url('assets/pages/jquery.form-pickers.init.js') }}"></script>
    <script src="{{ url('plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ url('plugins/switchery/js/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('plugins/multiselect/js/jquery.multi-select.js') }}"></script>
    <script type="text/javascript" src="{{ url('plugins/jquery-quicksearch/jquery.quicksearch.js') }}"></script>
    <script src="{{ url('plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>

    <script type="text/javascript" src="{{ url('assets/pages/jquery.form-advanced.init.js') }}"></script>
    <!-- Required datatable js -->
    <script src="{{ url('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <!-- Buttons examples -->
    <script src="{{ url('plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ url('plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ url('plugins/datatables/jszip.min.js') }}"></script>
    <script src="{{ url('plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ url('plugins/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ url('plugins/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ url('plugins/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ url('plugins/datatables/buttons.colVis.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var APP_URL = {!! json_encode(url('/')) !!};

            var DefaultTglAwal = $('#from_filter_date').val();
            var DefaultTglAkhir = $('#to_filter_date').val();
            var accountID = "{{ Session::get('agent_vnid') }}";
            var category = $('#category').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).ajaxStart(function() {
                $("#loading").show();
                }).ajaxStop(function() {
                $("#loading").hide();
            });
            load_data();

            function load_data(tglAwal = DefaultTglAwal, tglAkhir = DefaultTglAkhir)
            {
                        var dmgkpi = $('#dmgkpi').DataTable({
                                    processing: true,
                                    serverside: true,
                                    searching: false,
                                    ordering: false,
                                    destroy: true,
                                    dom: 'lrtip',
                                    dom: 'Bfrtip',
                                    buttons: [
                                        {
                                        extend: 'excel',
                                        autoFilter: true,
                                        exportOptions : {
                                                columns:  [ 0, 1, 2, 3, 4, 5, 6, 7],
                                            }               
                                        },
                                        {
                                        extend: 'print',
                                        autoFilter: true,
                                        exportOptions : {
                                            columns:  [ 0, 1, 2, 3, 4, 5, 6, 7],
                                            }               
                                        }
                                    ],
                                    ajax: {
                                        method : 'GET',
                                        url : APP_URL+"/datadetdmgkpi",
                                        dataType : 'json',
                                        dataSrc: "",
                                        data: {'accountID': accountID, 'category': category, 'from': tglAwal, 'to': tglAkhir},
                                    },
                                    columns: [
                                        {
                                            "data": null,
                                            render: function (data, type, row, meta) {
                                                return meta.row + meta.settings._iDisplayStart + 1;
                                            }
                                        },
                                        {
                                            data:'trnNoHAWB'
                                        },
                                        {
                                            data:'trnOrg'
                                        },
                                        {
                                            data:'trnDest'
                                        },
                                        {
                                            data:'trnPickupDate'
                                        },
                                        {
                                            data:'trnchargeswt'
                                        },
                                        {
                                            data:'trnkoli'
                                        },
                                        {
                                            data:'dmg_kpi'
                                        }
                                    ],
                                    columnDefs: [
                                        {"className": "dt-center", "targets": "_all"}
                                    ]
                        });

                $("#dmgkpi_excel").on("click", function() {
                    dmgkpi.button('.buttons-excel').trigger();
                });
                $("#dmgkpi_print").on("click", function() {
                    dmgkpi.button('.buttons-print').trigger();
                }); 
            }
            
            $('#FilterTgl').click(function(){
                var tglAwal = $('#from_filter_date').val();
                var tglAkhir = $('#to_filter_date').val();
                    $('#dmgkpi').DataTable().destroy();
                    load_data(tglAwal, tglAkhir);
                $('#Filter-modal').modal('hide');
            })
                        

            // $('#FilterTgl').click(function(){
            //                 var tglAwal = $('#from_filter_date').val();
            //                 var tglAkhir = $('#to_filter_date').val();

            //                 $.ajax({
            //                     url: '/datashpkpi',
            //                     method: 'GET',
            //                     dataType: 'json',
            //                     data: {'accountID': accountID, 'category': category, 'from': tglAwal, 'to': tglAkhir},
            //                     success: function (d) {
            //                         var tes = Array(d.success, d.failed, d.ondel);
            //                         var total = d.total;
            //                         new Chart(shipmentkpi, {
            //                             type: 'bar',
            //                             data: {
            //                                 ykeys: ['Achieve', 'Failed', 'On Delivery'],
            //                                 labels: ['Achieve', 'Failed', 'On Delivery'],
            //                                 datasets: [{
            //                                     label: '',
            //                                     data: Object.values(tes),
            //                                     backgroundColor: [
            //                                         '#ade498',
            //                                         '#f4668f',
            //                                         '#808088',
            //                                     ],
            //                                     borderColor: [
            //                                         '#ade498',
            //                                         '#f4668f',
            //                                         '#808088',
            //                                     ],
            //                                     borderWidth: 1
            //                                 }]
            //                             },
            //                             options: {
            //                                 legend: {
            //                                     display: false
            //                                 },
            //                                 scales: {
            //                                     yAxes: [{
            //                                         ticks: {
            //                                             beginAtZero: true,
            //                                             min: 0,
            //                                             max: 500,
            //                                             stepSize: 100,
            //                                         }
            //                                     }],
            //                                     xAxes: [{
            //                                         // Change here
            //                                         barPercentage: 0.5
            //                                     }]
            //                                 },
            //                                 animation: {
            //                                 duration: 1,
            //                                 onComplete: function() {
            //                                     var chartInstance = this.chart,
            //                                     ctx = chartInstance.ctx;

            //                                     ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            //                                     ctx.textAlign = 'center';
            //                                     ctx.textBaseline = 'bottom';

            //                                     this.data.datasets.forEach(function(dataset, i) {
            //                                     var meta = chartInstance.controller.getDatasetMeta(i);
            //                                     meta.data.forEach(function(bar, index) {
            //                                         if (dataset.data[index] > 0) {
            //                                         var data = dataset.data[index];
            //                                         ctx.fillText(data, bar._model.x, bar._model.y);
            //                                         }
            //                                     });
            //                                     });
            //                                 }
            //                                 },
            //                                 tooltips: {       
            //                                         callbacks: {
            //                                             title: function(tooltipItem, data) {
            //                                                 return '';
            //                                             },
            //                                             label: function(tooltipItem, data) {
            //                                                 return 'Qty : ' + data['datasets'][0]['data'][tooltipItem['index']];
            //                                             },
            //                                             afterLabel: function(tooltipItem, data) {
            //                                                 var percent = Math.round(data['datasets'][0]['data'][tooltipItem['index']] / total * 100).toFixed(2);
            //                                                 return '(' + percent + '%)';
            //                                             },
            //                                         },
            //                                 position: 'average',
            //                                 xPadding: 15,
            //                                 yPadding: 15,
            //                                 },
            //                             },
            //                         })
            //                         $('#shp_total').html(d.total);
            //                     }
            //                 })

                
            // })                     
                    

                // $('#reset_report').click(function(){
                //     $('#from_datereport').val('');
                //     $('#to_datereport').val('');
                //     $('#select_dest').val('all');
                //     $('#select_ho').val('all');
                //     $('#textReport_detail').css('display', 'block');
                //     $('#detailReport-awb').css('display', 'none');
                // });

               

                // $(document).on('click', '.infoReport', function(){
                //     var id = $(this).attr("id");
                //     $.ajax({
                //         url:"/Json/awb",
                //         method:'GET',
                //         data:{id:id},
                //         dataType:'json',
                //         success:function(data)
                //         {
                //             ReportsuccessFunction(data);
                            
                //         }
                //     })
                // });

                // function ReportsuccessFunction(data) {
                //     $('#textReport_detail').css('display', 'none');
                //     $('#detailReport-awb').css('display', 'block');
                //     $.each(data, function(i, val) {
                //         $('#report_awb').val(": " + val.trnNoHAWB);
                //         $('#report_org').val(": " + val.trnOrg);
                //         $('#report_dest').val(": " + val.trnDest);
                //         $('#report_srv').val(": " + val.trnTypeOfService);
                //         $('#report_cs').val(": " + val.cltbuser_usrId);
                //     })
                // }

                // $.ajax({
                //         method: 'GET',
                //         url: '/Json/dest',
                //         dataType: 'json',
                //         data: {id: 'id'},
                //         success: function(data) {
                //             $.each(data, function(i, val) {
                //                 $('#select_dest').append('<option value="' + val.id + '">' + val.id + '</option>');
                //             })
                //         }
                // });
        });

    </script>
@endsection