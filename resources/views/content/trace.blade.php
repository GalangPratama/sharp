@extends('master.template')

@section('css')
    <link href="{{ url('plugins/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
    <link href="{{ url('plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
    <link href="{{ url('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ url('plugins/clockpicker/css/bootstrap-clockpicker.min.css') }}" rel="stylesheet">
    <link href="{{ url('plugins/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
    <link href="{{ url('plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
    <!-- DataTables -->
    <link href="{{ url('plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-lg-12 col-xl-12">
        <div class="col-6  app-search" style="margin: auto">
            <form role="search" class="search_awb">
                <input type="text" placeholder="Search..." class="form-control" name="awb" id="awb" autocomplete="off">
                <button type="submit"><i class="fa fa-search"></i></button>
            </form>
        </div>
        <div class="col-6 data-false badge-danger">
        </div>

        <div class="data-true">
            <div class="col-12">
                <div class="card-box">
                    <table class="table" cellspacing="0" width="100%">
                        <thead class="thead-default" style="text-align: center;">
                            <tr>
                                <th style="text-align: center;">AWB</th>
                                <th style="text-align: center;">No. DO WMS</th>
                                <th style="text-align: center;">Service</th>
                                <th style="text-align: center;">Tujuan</th>
                                <th style="text-align: center;">Penerima</th>
                                <th style="text-align: center;">Tgl Diterima</th>
                                <th style="text-align: center;">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="text-align: center;" id="no_awb"></td>
                                <td style="text-align: center;" id="no_do"></td>
                                <td style="text-align: center;" id="service"></td>
                                <td style="text-align: center;" id="receiver_address"></td>
                                <td style="text-align: center;" id="receiver_name"></td>
                                <td style="text-align: center;" id="pod_date"></td>
                                <td style="text-align: center;" id="status"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-12">
                <div class="card-box">
                    <h5>Detail POD</h5>
                    <table class="table" id="table-history">
                        <thead class="thead-default">
                            <tr>
                                <th>Tanggal</th>
                                <th>Status</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody id="history">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div> 
</div>
<div id="loading" style="display: none;"> 
    <div class="loader">
        <img src="{{ url('assets/images/sp-loading2.gif') }}" alt="loading">
    </div>
</div>

@endsection

@section('js')
    <script src="{{ url('plugins/moment/moment.js') }}"></script>
    <script src="{{ url('plugins/timepicker/bootstrap-timepicker.js') }}"></script>
    <script src="{{ url('plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{ url('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ url('plugins/clockpicker/js/bootstrap-clockpicker.min.js') }}"></script>
    <script src="{{ url('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ url('assets/pages/jquery.form-pickers.init.js') }}"></script>
    <script src="{{ url('plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ url('plugins/switchery/js/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('plugins/multiselect/js/jquery.multi-select.js') }}"></script>
    <script type="text/javascript" src="{{ url('plugins/jquery-quicksearch/jquery.quicksearch.js') }}"></script>
    <script src="{{ url('plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>

    <script type="text/javascript" src="{{ url('assets/pages/jquery.form-advanced.init.js') }}"></script>
    <!-- Required datatable js -->
    <script src="{{ url('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <!-- Buttons examples -->
    <script src="{{ url('plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ url('plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ url('plugins/datatables/jszip.min.js') }}"></script>
    <script src="{{ url('plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ url('plugins/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ url('plugins/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ url('plugins/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ url('plugins/datatables/buttons.colVis.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var APP_URL = {!! json_encode(url('/')) !!};

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.search_awb').submit(function(e){
                $(document).ajaxStart(function() {
                    $("#loading").show();
                    }).ajaxStop(function() {
                    $("#loading").hide();
                });
                
                e.preventDefault();
                var awb = $("#awb").val();
                    $.ajax({
                        url: APP_URL+"/trace/awb",
                        method: 'GET',
                        data: {'awb' : awb},
                        dataType: 'json',
                        success:function(res){                          
                            var data1 = res.total.status;
                            if (data1.code == 200) {
                                var data2 = res.total.results;
                                var data4 = res.total.results.last_status;
                                $(".data-false").css("display", "none");
                                $(".data-true").css("display", "block");

                                $("#no_awb").html(data2.waybill_number);
                                $("#no_do").html(data2.custref);
                                $("#service").html(data2.service);
                                $("#receiver_address").html(data2.receiver_address);
                                $("#receiver_name").html(data2.receiver_name);
                                $("#pod_date").html(data2.POD_receiver_time);
                                $("#status").html(data4.status);
                                $("#table-history").find("tbody").empty();
                                
                                $.each(data2.track_history, function(i, val) {
                                    $("#history").append("<tr><td>"+val.date_time+"</td><td>"+val.status+" ["+val.city+"]</td><td>"+val.desc+"</td></tr>");
                                })
                            } else {
                                $(".data-true").css("display", "none");
                                $(".data-false").css("display", "block");
                                $(".data-false").html("<h5>Data tidak ditemukan</h5>");
                            }
                            // console.log(res.total.status.code);
                        },
                    })
            })
        });

    </script>
@endsection