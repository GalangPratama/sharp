<div class="topbar">

    <!-- LOGO -->
    <div class="topbar-left">
        <div class="text-center">
            {{-- <a href="index.html" class="logo"><img src="{{ url('assets/images/logo-total.png') }}" alt="" width="200"></a> --}}
            <!-- Image Logo here -->
         {{-- <a href="index.html" class="logo">
            <i class="icon-c-logo"><h1 style="height: 100%; padding: 13px 0; letter-spacing: 8px; font-family:monospace">T</h1></i>
            <span><h1 style="height: 100%; padding: 13px 0; letter-spacing: 8px; font-family:monospace">TMS</h1></span>
            
        </a> --}}
        </div>
    </div>

    <!-- Button mobile view to collapse sidebar menu -->
    <nav class="navbar-custom">

        <ul class="list-inline float-right mb-0">
            {{-- <li class="list-inline-item dropdown notification-list">
                <a class="nav-link dropdown-toggle arrow-none waves-light waves-effect" data-toggle="dropdown" href="#" role="button"
                   aria-haspopup="false" aria-expanded="false">
                    <i class="dripicons-bell noti-icon"></i>
                    <span class="badge badge-pink noti-icon-badge">4</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-lg" aria-labelledby="Preview">
                    <!-- item-->
                    <div class="dropdown-item noti-title">
                        <h5><span class="badge badge-danger float-right">5</span>Notification</h5>
                    </div>

                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <div class="notify-icon bg-success"><i class="icon-bubble"></i></div>
                        <p class="notify-details">Robert S. Taylor commented on Admin<small class="text-muted">1 min ago</small></p>
                    </a>

                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <div class="notify-icon bg-info"><i class="icon-user"></i></div>
                        <p class="notify-details">New user registered.<small class="text-muted">1 min ago</small></p>
                    </a>

                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <div class="notify-icon bg-danger"><i class="icon-like"></i></div>
                        <p class="notify-details">Carlos Crouch liked <b>Admin</b><small class="text-muted">1 min ago</small></p>
                    </a>

                    <!-- All-->
                    <a href="javascript:void(0);" class="dropdown-item notify-item notify-all">
                        View All
                    </a>

                </div>
            </li>

            <li class="list-inline-item notification-list">
                <a class="nav-link waves-light waves-effect" href="#" id="btn-fullscreen">
                    <i class="dripicons-expand noti-icon"></i>
                </a>
            </li>

            <li class="list-inline-item notification-list">
                <a class="nav-link right-bar-toggle waves-light waves-effect" href="#">
                    <i class="dripicons-message noti-icon"></i>
                </a>
            </li> --}}
            <li class="list-inline-item notification-list">
                <a class="nav-link waves-light waves-effect" href="#" id="btn-fullscreen">
                    <i class="dripicons-expand noti-icon"></i>
                </a>
            </li>
            <li class="list-inline-item dropdown notification-list">
                <a href="#" class="btn waves-effect waves-light" data-toggle="modal" data-target="#Filter-modal">
                   <i class="dripicons-gear noti-icon"></i>
                </a>
            </li>
            <li class="list-inline-item dropdown notification-list" style="color: #fff;" id="clock">
            </li>

        </ul>

        <ul class="list-inline menu-left mb-0 text-center">
            {{-- <li class="float-left">
                <button class="button-menu-mobile open-left waves-light waves-effect">
                    <i class="dripicons-menu"></i>
                </button>
            </li> --}}
            {{-- <li class="hide-phone app-search">
                
            </li> --}}
            <img src="{{ url('assets/images/tl.png') }}" alt="nissan" width="200"> 
        </ul>

    </nav>

</div>
<!-- Top Bar End -->