<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>
               
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect text-center">
                        <span>
                            <img src="assets/images/users/avatar-11.png" alt="user" class="rounded-circle" width="100">
                        </span>
                        <h5>
                            {{Session::get('username')}}
                        </h5>
                    </a>
                    <ul class="list-unstyled">
                        <li><a href="{{ url('#') }}">Profil</a></li>
                        <li><a href="{{ url('#') }}">Setting</a></li>
                        <li><a href="{{ url('/logout') }}">Logout</a></li>
                    </ul>
                </li>
                <li class="text-muted menu-title">Main Menu</li>

                <li class="has_sub">
                    <a href="{{ route("dashboard") }}" class="waves-effect"><i class="ti-agenda"></i> <span> Dashboard </span></a>
                </li>

                <li class="has_sub">
                    <a href="{{ route("trace") }}" class="waves-effect"><i class="ti-search"></i> <span> Tracing AWB </span></a>
                </li>

                {{-- <li class="has_sub">
                    <a href="{{ url('/report') }}" class="waves-effect"><i class="ti-paint-bucket"></i> <span> Reporting </span></a>
                </li> --}}

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-agenda"></i><span> Report Waybill</span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{ route("shipmentKPI") }}" target="_blank">All Shipment</a></li>
                        <li><a href="{{ route("dailyshipment") }}" target="_blank">Daily Shipment</a></li>
                    </ul>
                </li>

                {{-- <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-spray"></i> <span> Billing Received </span> </a>
                </li> --}}

            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>