<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="shortcut icon" href="{{ url('assets/images/users/avatar-12.png') }}">

        <title>TMS | {{ $title }}</title>

        <!--Morris Chart CSS -->
        <link href="{{ url('plugins/custombox/css/custombox.css') }}" rel="stylesheet">
        <link href="{{ url('plugins/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
        <link href="{{ url('plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
        <link href="{{ url('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
        <link href="{{ url('plugins/clockpicker/css/bootstrap-clockpicker.min.css') }}" rel="stylesheet">
        <link href="{{ url('plugins/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">        
        <link rel="stylesheet" href="{{ url('plugins/select2/css/select2.min.css') }}">
        @yield('css')
        <link href="{{ url('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/css/style.css') }}" rel="stylesheet" type="text/css" />

        <script src="{{ url('assets/js/modernizr.min.js') }}" type="text/css"></script>


    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
                @include('master.header')


            <!-- ========== Left Sidebar Start ========== -->
                @include('master.sidebar')
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                            @yield('content')
                        <!-- end row -->


                    </div> <!-- container -->

                </div> <!-- content -->

                @include('master.footer')
            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

        </div>
        <!-- END wrapper -->

        

        <div class="modal fade" id="Filter-modal" role="dialog">
            <div class="modal-dialog">
         
             <!-- Modal content-->
                <div class="modal-content">
                    <form id="FilterForm">
                        <div class="modal-header" style="text-align: center;">
                            <h4 class="modal-title">Filter Report</h4>
                            <button type="button" class="close" data-dismiss="modal"></button>
                        </div>
                        <div class="modal-text">
                            <div class="row">
                                <div class="col-md-12" style="display: none;">
                                    <h5>Select Category</h5>
                                    <select class="form-control" name="category" id="category">
                                        <option value="all">All</option>
                                        <option value="">Consumable</option>
                                        <option value="">Demo</option>
                                        <option value="">Device</option>
                                        <option value="">Other</option>
                                    </select>    
                                </div>
                                <div class="col-md-12 row" style="margin-top: 20px;">
                                    <div class="col-md-12">
                                        <h5>Select Periode</h5>
                                    </div>
                                    <div class="input-group col-md-6">
                                        <input type="text" class="form-control" placeholder="From" id="from_filter_date">
                                        <span class="input-group-addon bg-custom b-0"><i class="md md-event-note text-white"></i></span>
                                    </div><!-- input-group -->
                                
                                    <div class="input-group col-md-6">
                                        <input type="text" class="form-control" placeholder="To" id="to_filter_date">
                                        <span class="input-group-addon bg-custom b-0"><i class="md md-event-note text-white"></i></span>
                                    </div><!-- input-group -->
                                </div>
                            </div>
                        </div>
                    </form>
                    
                    <div class="modal-footer">
                        <button class="btn btn-default" id="FilterDate">Filter <i class="md-tune"></i></button>
                    </div>
                </div>
            </div>
        </div>

        
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="{{ url('assets/js/jquery.min.js') }}"></script>
        <script src="{{ url('assets/js/popper.min.js') }}"></script><!-- Popper for Bootstrap -->
        <script src="{{ url('assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ url('assets/js/detect.js') }}"></script>
        <script src="{{ url('assets/js/fastclick.js') }}"></script>
        <script src="{{ url('assets/js/jquery.slimscroll.js') }}"></script>
        <script src="{{ url('assets/js/jquery.blockUI.js') }}"></script>
        <script src="{{ url('assets/js/waves.js') }}"></script>
        <script src="{{ url('assets/js/wow.min.js') }}"></script>
        <script src="{{ url('assets/js/jquery.nicescroll.js') }}"></script>
        <script src="{{ url('assets/js/jquery.scrollTo.min.js') }}"></script>
        <script src="{{ url('plugins/peity/jquery.peity.min.js') }}"></script>        
        
        <!-- jQuery  -->
        <script src="{{ url('plugins/waypoints/lib/jquery.waypoints.min.js') }}"></script>
        <script src="{{ url('plugins/counterup/jquery.counterup.min.js') }}"></script>
        <script src="{{ url('plugins/jquery-knob/jquery.knob.js') }}"></script>
        <script src="{{ url('plugins/select2/js/select2.min.js') }}"></script>
        <script src="{{ url('assets/js/jquery.core.js') }}"></script>
        <script src="{{ url('assets/js/jquery.app.js') }}"></script>

        @yield('js')
        <script src="{{ url('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ url('plugins/custombox/js/custombox.min.js') }}"></script>
        <script src="{{ url('plugins/custombox/js/legacy.min.js') }}"></script>
        <script type="text/javascript">
        
        function time(){
            var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            var date = new Date();
            var date1 = date.getDate();
            var date2 = months[date.getMonth()];
            var date3 = date.getFullYear();
            var hours = addZero(date.getHours());
            var minuts = addZero(date.getMinutes());
            var second = addZero(date.getSeconds());

            document.getElementById('clock').innerHTML = date1+" "+date2+" "+date3+" "+hours+":"+minuts+":"+second;
            setTimeout(time, 1000);
            return true;
        }
                jQuery('#datepicker').datepicker();
                jQuery('#from_filter_date').datepicker({
					format: "yyyy-mm-dd",
                	autoclose: true,
                	todayHighlight: true
				});

				jQuery('#to_filter_date').datepicker({
					format: "yyyy-mm-dd",
                	autoclose: true,
                    todayHighlight: true,                    
                });
                var d = new Date();
                var currMonth = d.getMonth();
                var currYear = d.getFullYear();
                var currDate = d.getDate();
                var startDate = new Date(currYear, currMonth, 1);
                var endDate = new Date(currYear, currMonth, 31);

                jQuery('#from_filter_date').datepicker("setDate", startDate);
                jQuery('#to_filter_date').datepicker("setDate", endDate);
        </script>

    </body>
</html>