<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="{{ url('assets/images/users/avatar-12.png') }}">

        <title>TMS | {{ $title }}</title>
        
        <link href="{{ url('plugins/custombox/css/custombox.css') }}" rel="stylesheet">
        <link href="{{ url('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/css/style.css') }}" rel="stylesheet" type="text/css" />
        
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>  
        <script src="{{ url('assets/js/modernizr.min.js') }}"></script>
        
    </head>
    <body>

        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page1">
            <div class="card-box1">
                <div class="panel-heading text-center">
                    <img src="{{ url('assets/images/logo-total.png') }}" alt="" width="350">
                </div>

                {{-- action="{{ url('/checkLogin') }}" method="GET" --}}
                <div class="p-20">
                    <form class="form-horizontal">
                        {{ csrf_field() }}
                            <h5 style="color:tomato; text-align:center;" id="errorLogin"></h5>
                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username" class="col-md-6 control-label">Username</label>

                            <div class="col-md-12">
                                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" autofocus autocomplete="off">

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-12 control-label">Password</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-center m-t-40">
                            <div class="col-12">
                                <button id="LoginCheck" class="btn btn-default btn-block text-uppercase waves-effect waves-light"
                                        type="button">Log In
                                </button>
                                {{-- <a href="#custom-modal" class="btn btn-primary waves-effect waves-light" data-animation="blur" data-plugin="custommodal"
                                                       data-overlaySpeed="100" data-overlayColor="#36404a">Show Me</a> --}}
                            </div>
                        </div>
                    </form>
                    
                    
                </div>
            </div>
            
        </div>
        
        
        <div id="loading" style="display: none;"> 
            <div class="loader">
                <img src="{{ url('assets/images/sp-loading2.gif') }}" alt="loading">
            </div>
        </div>

        <div class="modal fade" id="customer-modal" role="dialog">
            <div class="modal-dialog">
         
             <!-- Modal content-->
             <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Select Customer</h4>
              </div>
                <div class="modal-body">
                    <select class="form-control" name="customerid" id="customerid">
                        <option disabled selected value>Select Customer..</option>
                    </select>
                </div>
              <div class="modal-footer">
               <button type="submit" class="btn btn-success" id="checkAccountCust">Submit</button>
              </div>
             </div>
            </div>
        </div>
        

    	<script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="{{ url('assets/js/jquery.min.js') }}"></script>
        <script src="{{ url('assets/js/popper.min.js') }}"></script><!-- Popper for Bootstrap -->
        <script src="{{ url('assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ url('assets/js/detect.js') }}"></script>
        <script src="{{ url('assets/js/fastclick.js') }}"></script>
        <script src="{{ url('assets/js/jquery.slimscroll.js') }}"></script>
        <script src="{{ url('assets/js/jquery.blockUI.js') }}"></script>
        <script src="{{ url('assets/js/waves.js') }}"></script>
        <script src="{{ url('assets/js/wow.min.js') }}"></script>
        <script src="{{ url('assets/js/jquery.nicescroll.js') }}"></script>
        <script src="{{ url('assets/js/jquery.scrollTo.min.js') }}"></script>
        
        <script src="{{ url('plugins/custombox/js/custombox.min.js') }}"></script>
        <script src="{{ url('assets/js/jquery.core.js') }}"></script>
        <script src="{{ url('assets/js/jquery.app.js') }}"></script>
        <script>
            $(document).ready(function() {
                var APP_URL = {!! json_encode(url('/')) !!}
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                $('#LoginCheck').click(function(){
                    $(document).ajaxStart(function() {
                        $("#loading").show();
                        }).ajaxStop(function() {
                        $("#loading").hide();
                    });

                    var username = $('#username').val();
                    var password = $('#password').val(); 
                    if (username != '' && password != '') {
                        $.ajax({
                        url: APP_URL+"/cekLogin",
                        method:'GET',
                        data:{'username':username, 'password':password},
                        dataType:'json',
                        success:function(res)
                        {
                                if (res.success == 200) {
                                    // window.location.href = "{{ url('/dashboard') }}";
                                    if (res.data.agent_vnid == "ALL") {
                                        var customerID = res.data.agent_vnid;
                                        $('#customer-modal').modal('show');
                                        $("#loading").hide();
                                        $.ajax({
                                            url: APP_URL+"/customercode",
                                            method:'GET',
                                            data:{'customerID':customerID},
                                            dataType:'json',
                                            success:function(res)
                                            {
                                                $.each(res, function(i, val) {
                                                    $("#customerid").append("<option value='"+val.csAcc+"'>"+val.csName+"</option>")
                                                })
                                            }
                                        })
                                    } else{
                                        window.location.href = "{{ url('/dashboard') }}";
                                    }
                                } else {
                                    // console.log(response);
                                    $('#errorLogin').html("Username atau Password Salah");
                                }
                        }
                    })
                    } else {
                        swal("", "Masukan Username dan Password", "warning");
                    }
                })
            });
                $('#checkAccountCust').click(function(){
                    $('#customer-modal').modal('hide');
                    var APP_URL = {!! json_encode(url('/')) !!}
                    var id = $("#customerid").val();
                    var username = $('#username').val();
                    var password = $('#password').val(); 
                        $.ajax({
                            url: APP_URL+"/cekLogin",
                            method:'GET',
                            data:{'username':username, 'password':password, 'id' : id},
                            dataType:'json',
                            success:function(res)
                            {
                                window.location.href = "{{ url('/dashboard') }}";
                            }
                        })
                })
        </script>
	
	</body>
</html>