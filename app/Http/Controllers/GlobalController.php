<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use ApiCurl;

class GlobalController extends Controller
{
    protected $layout = "master.template";

    public function index(){
        return redirect('/dashboard');
    }
    
    public function login(){
        $title = 'Login';
        return view('login')->with(['title'=> $title]);
    }

    public function username(Request $request){
        $user = $request->input('username');
        $data = ApiCurl::get_username($user);
        return $data;
    }

    public function customer(Request $request){
        $id = $request->input('customerID');
        $data = ApiCurl::get_customer($id);
        return $data;
    }

    public function get_dashboard(Request $request){
        $accountid = $request->input('accountID');
        $category = $request->input('category');
        $tglawal = $request->input('from');
        $tglakhir = $request->input('to');
        $data = ApiCurl::getDashboard($accountid, $category, $tglawal, $tglakhir);
        return $data;
    }

    public function get_shpkpi(Request $request){
        $accountid = $request->input('accountID');
        $category = $request->input('category');
        $tglawal = $request->input('from');
        $tglakhir = $request->input('to');
        $data = ApiCurl::getShpKpi($accountid, $category, $tglawal, $tglakhir);
        return $data;
    }

    public function get_bycatshp(Request $request){
        $accountid = $request->input('accountID');
        $category = $request->input('category');
        $tglawal = $request->input('from');
        $tglakhir = $request->input('to');
        $shp = "shp";
        $data = ApiCurl::getBycatShp($accountid, $category, $tglawal, $tglakhir, $shp);
        return $data;
    }

    public function get_dmgkpi(Request $request){
        $accountid = $request->input('accountID');
        $category = $request->input('category');
        $tglawal = $request->input('from');
        $tglakhir = $request->input('to');
        $data = ApiCurl::getDmgKpi($accountid, $category, $tglawal, $tglakhir);
        return $data;
    }

    public function get_ivc(Request $request){
        $accountid = $request->input('accountID');
        $data = ApiCurl::getIvc($accountid);
        return $data;
    }

    public function get_bycatdmg(Request $request){
        $accountid = $request->input('accountID');
        $category = $request->input('category');
        $tglawal = $request->input('from');
        $tglakhir = $request->input('to');
        $shp = "dmg";
        $data = ApiCurl::getBycatDmg($accountid, $category, $tglawal, $tglakhir, $shp);
        return $data;
    }

    public function get_detdmgkpi(Request $request){
        $accountid = $request->input('accountID');
        $category = $request->input('category');
        $tglawal = $request->input('from');
        $tglakhir = $request->input('to');
        $data = ApiCurl::getDetDmgKpi($accountid, $category, $tglawal, $tglakhir);
        return $data;
    }

    public function get_grapall(Request $request){
        $accountid = $request->input('accountID');
        $category = $request->input('category');
        $tglawal = $request->input('from');
        $tglakhir = $request->input('to');
        $data = ApiCurl::getGrapAll($accountid, $category, $tglawal, $tglakhir);
        return $data;
    }

    public function get_grapdealy(Request $request){
        $accountid = $request->input('accountID');
        $category = $request->input('category');
        $tglawal = $request->input('from');
        $tglakhir = $request->input('to');
        $data = ApiCurl::getGrapDealy($accountid, $category, $tglawal, $tglakhir);
        return $data;
    }

    public function get_shpdst(Request $request){
        $accountid = $request->input('accountID');
        $category = $request->input('category');
        $tglawal = $request->input('from');
        $tglakhir = $request->input('to');
        $data = ApiCurl::getGrapDst($accountid, $category, $tglawal, $tglakhir);
        return $data;
    }

    public function get_graporg(Request $request){
        $accountid = $request->input('accountID');
        $category = $request->input('category');
        $tglawal = $request->input('from');
        $tglakhir = $request->input('to');
        $data = ApiCurl::getGrapOrg($accountid, $category, $tglawal, $tglakhir);
        return $data;
    }
    
    public function PostLogin(Request $request){
        $user = $request->input('username');
        $pass = $request->input('password');
        $customerid = $request->input('id');;
        $data = $this->username($request);
        foreach($data as $i){   
            if($i != null && $pass == $i['usrPass']){
                if ($i['agent_vnid'] == "ALL") {
                    Session::put('id',$i['usrId']);
                    Session::put('username',$i['usrName']);
                    Session::put('agent_vnid',$customerid);
                    Session::put('login',TRUE);
                    return response()->json([
                        'success' => 200,
                        'data' => $i,
                    ]);
                }
                Session::put('id',$i['usrId']);
                Session::put('username',$i['usrName']);
                Session::put('agent_vnid',$i['agent_vnid']);
                Session::put('login',TRUE);
                return response()->json([
                    'success' => 200,
                    'data' => $i,
                ]);
            }else{
                return response()->json([
                    'success' => 500,
                    'message' => 'Login Gagal!'
                ]);
            }
        }
    }
    
    public function dashboard(){
        $title = 'Dashboard';
        return view('welcome')->with(['title'=> $title]);
    }

    public function shipment_kpi(){
        $title = 'Shipment KPI';
        return view('content.shipmentKpi', ['title'=> $title]);
    }

    public function damage_kpi(){
        $title = 'Damage KPI';
        return view('content.damageKpi', ['title'=> $title]);
    }

    public function daily_shipment(){
        $title = 'Daily Shipment';
        return view('content.daily', ['title'=> $title]);
    }

    public function trace_awb(){
        $title = 'Tracing AWB';
        return view('content.trace', ['title'=> $title]);
    }

    public function get_traceawb(Request $request){
        $awb = $request->input('awb');
        $data = ApiCurl::traceawb($awb);
        return response($data);
    }

    public function Logout(){
        Session::flush();
        return redirect('/login');
    }
}