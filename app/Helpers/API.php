<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;

class API {
    public static function get_username($user_id) {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://202.138.229.86/tics/apiportal/data/user/".$user_id,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
                'x-token: 00043eb6617434cc5f357bbf692e53b3'
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response, TRUE);
        }
    }

    public static function get_customer($id) {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://202.138.229.86/tics/api/data/custdata/".$id,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
                'x-token: 00043eb6617434cc5f357bbf692e53b3'
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response, TRUE);
        }
    }

    public static function getDashboard($accountid, $category, $tglawal, $tglakhir){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://202.138.229.86/tics/apiportal/web/dashboard/".$accountid."/".$category."/".$tglawal."/".$tglakhir,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
                'x-token: 00043eb6617434cc5f357bbf692e53b3'
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response, TRUE);
        }
    }

    public static function getShpKpi($accountid, $category, $tglawal, $tglakhir){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://202.138.229.86/tics/apiportal/web/shp_kpi/".$accountid."/".$category."/".$tglawal."/".$tglakhir,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
                'x-token: 00043eb6617434cc5f357bbf692e53b3'
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response, TRUE);
        }
    }

    public static function getBycatShp($accountid, $category, $tglawal, $tglakhir, $shp){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://202.138.229.86/tics/apiportal/reasons/bycat/".$accountid."/".$category."/".$shp."/".$tglawal."/".$tglakhir,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
                'x-token: 00043eb6617434cc5f357bbf692e53b3'
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response, TRUE);
        }
    }

    public static function getDmgKpi($accountid, $category, $tglawal, $tglakhir){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://202.138.229.86/tics/apiportal/web/dmg_kpi/".$accountid."/".$category."/".$tglawal."/".$tglakhir,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
                'x-token: 00043eb6617434cc5f357bbf692e53b3'
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response, TRUE);
        }
    }

    public static function getIvc($accountid){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://202.138.229.86/tics/apiportal/web/invkpi/".$accountid,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
                'x-token: 00043eb6617434cc5f357bbf692e53b3'
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response, TRUE);
        }
    }

    public static function getBycatDmg($accountid, $category, $tglawal, $tglakhir, $shp){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://202.138.229.86/tics/apiportal/reasons/bycat/".$accountid."/".$category."/".$shp."/".$tglawal."/".$tglakhir,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
                'x-token: 00043eb6617434cc5f357bbf692e53b3'
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response, TRUE);
        }
    }

    public static function getDetDmgKpi($accountid, $category, $tglawal, $tglakhir){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://202.138.229.86/tics/apiportal/web/detdamage/".$accountid."/".$category."/".$tglawal."/".$tglakhir,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
                'x-token: 00043eb6617434cc5f357bbf692e53b3'
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response, TRUE);
        }
    }

    public static function getGrapAll($accountid, $category, $tglawal, $tglakhir){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://202.138.229.86/tics/apiportal/web/graphic_all/".$accountid."/".$category."/".$tglawal."/".$tglakhir,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
                'x-token: 00043eb6617434cc5f357bbf692e53b3'
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response, TRUE);
        }
    }

    public static function getGrapDealy($accountid, $category, $tglawal, $tglakhir){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://202.138.229.86/tics/apiportal/web/graphic_day/".$accountid."/".$category."/".$tglawal."/".$tglakhir,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
                'x-token: 00043eb6617434cc5f357bbf692e53b3'
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response, TRUE);
        }
    }

    public static function getGrapDst($accountid, $category, $tglawal, $tglakhir){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://202.138.229.86/tics/apiportal/web/graphic_dst/".$accountid."/".$category."/".$tglawal."/".$tglakhir,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
                'x-token: 00043eb6617434cc5f357bbf692e53b3'
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response, TRUE);
        }
    }
    
    public static function getGrapOrg($accountid, $category, $tglawal, $tglakhir){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://202.138.229.86/tics/apiportal/web/graphic_org/".$accountid."/".$category."/".$tglawal."/".$tglakhir,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
                'x-token: 00043eb6617434cc5f357bbf692e53b3'
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response, TRUE);
        }
    }

    public static function traceawb($awb) {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://eidapps.tora.co.id/api/general/index.php/track/".$awb,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
                'x-token: 00043eb6617434cc5f357bbf692e53b3'
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response, TRUE);
        }
    }
    
}