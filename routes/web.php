<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', 'GlobalController@login')->name('login');
Route::get('/cekLogin', 'GlobalController@PostLogin');
Route::get('/logout', 'GlobalController@Logout');

Route::group(['middleware' => 'customauth'], function () {
    Route::get('/', 'GlobalController@index');
    Route::get('/dashboard', 'GlobalController@dashboard')->name("dashboard");
    Route::get('/shipment_kpi', 'GlobalController@shipment_kpi')->name('shipmentKPI');
    Route::get('/damage_kpi', 'GlobalController@damage_kpi')->name('damageKPI');
    Route::get('/dailyShipment', 'GlobalController@daily_shipment')->name('dailyshipment');
    Route::get('/trace', 'GlobalController@trace_awb')->name("trace");
});

// ROUTE DATA API
Route::get('/datadashboard', 'GlobalController@get_dashboard');
Route::get('/datashpkpi', 'GlobalController@get_shpkpi');
Route::get('/databycatshp', 'GlobalController@get_bycatshp');
Route::get('/datadmgkpi', 'GlobalController@get_dmgkpi');
Route::get('/databycatdmg', 'GlobalController@get_bycatdmg');
Route::get('/datadetdmgkpi', 'GlobalController@get_detdmgkpi');
Route::get('/datagrapall', 'GlobalController@get_grapall');
Route::get('/datagrapdealy', 'GlobalController@get_grapdealy');
Route::get('/datashpdst', 'GlobalController@get_shpdst');
Route::get('/datagraporg', 'GlobalController@get_graporg');
Route::get('/dataivc', 'GlobalController@get_ivc');


Route::get('/customercode', 'GlobalController@customer');
Route::get('/trace/awb', 'GlobalController@get_traceawb');